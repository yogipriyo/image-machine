# IMAGE MACHINE - Mobile Application

### Overview
Image Machine is an iOS Mobile Application that will allow its user to create image machine and select images from their gallery to be associated with the image machine that has been created. It will be stored locally in user's device so it will still work without any internet connection.

This mobile application also has Scan QR code feature which will scan a qr code and if the qr code contain the value compared to the qr code number configured in a previously created image machine then it will navigate the user to the image machine details page.
  - Manage Image Machine (Create, edit & delete)
  - Manage images and connect them to the image machine
  - Use local device storage

### Techicals
Image machine was created using Swift 4.2 and XCode 10.0. Its local storage use the natively supported storage, Core Data. To save the space and resources, only the image asset local identifier which will be stored in the database. So it will not create additional image data and when the user delete the associated images for certain Image Machine, it will not delete the real image. It will only delete the related record in the database.

Image Machine uses a number of open source library to work properly :

* [Opal Image Picker](https://github.com/opalorange/OpalImagePicker) - A multiple image picker for iOS, written in Swift
* [Date Picker Dialog](https://github.com/opalorange/OpalImagePicker) - DatePickerDialog is an iOS drop-in classe that displays an UIDatePicker within an UIAlertView.

And of course Image Machine itself is open source with a [public repository](https://bitbucket.org/yogipriyo/image-machine/src/master/)
 on Bitbucket.

### Preparation

Make sure your computer has XCode 10 & Cocoapod installed.

Then navigate to the root directory and install the dependencies :

```sh
$ pod install
```

Then open the .workspace file using the XCode to start working on the Image Machine project

License
----

MIT


**Free Software, Hell Yeah!**

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)


   [dill]: <https://github.com/joemccann/dillinger>
   [git-repo-url]: <https://github.com/joemccann/dillinger.git>
   [john gruber]: <http://daringfireball.net>
   [df1]: <http://daringfireball.net/projects/markdown/>
   [markdown-it]: <https://github.com/markdown-it/markdown-it>
   [Ace Editor]: <http://ace.ajax.org>
   [node.js]: <http://nodejs.org>
   [Twitter Bootstrap]: <http://twitter.github.com/bootstrap/>
   [jQuery]: <http://jquery.com>
   [@tjholowaychuk]: <http://twitter.com/tjholowaychuk>
   [express]: <http://expressjs.com>
   [AngularJS]: <http://angularjs.org>
   [Gulp]: <http://gulpjs.com>

   [PlDb]: <https://github.com/joemccann/dillinger/tree/master/plugins/dropbox/README.md>
   [PlGh]: <https://github.com/joemccann/dillinger/tree/master/plugins/github/README.md>
   [PlGd]: <https://github.com/joemccann/dillinger/tree/master/plugins/googledrive/README.md>
   [PlOd]: <https://github.com/joemccann/dillinger/tree/master/plugins/onedrive/README.md>
   [PlMe]: <https://github.com/joemccann/dillinger/tree/master/plugins/medium/README.md>
   [PlGa]: <https://github.com/RahulHP/dillinger/blob/master/plugins/googleanalytics/README.md>

