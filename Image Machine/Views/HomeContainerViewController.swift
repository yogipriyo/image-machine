//
//  HomeContainerViewController.swift
//  Image Machine
//
//  Created by Yogi Priyo Prayogo on 10/12/18.
//  Copyright © 2018 Yogi Priyo Prayogo. All rights reserved.
//

import UIKit

enum TabBarMenu: String {
    case machineData = "Machine Data"
    case codeReader = "Code Reader"
}

class HomeContainerViewController: UIViewController {
    
    // MARK: Properties
    @IBOutlet weak var tabBarContainerView: UIView!
    @IBOutlet weak var machineDataButton: UIButton!
    @IBOutlet weak var machineDataBottomView: UIView!
    @IBOutlet weak var codeReaderButton: UIButton!
    @IBOutlet weak var codeReaderBottomView: UIView!
    @IBOutlet weak var contentContainerView: UIView!
    
    var activeTabBar: TabBarMenu = .machineData
    let machineDataListVC = MachineDataListViewController()
    let codeReaderVC = CodeReaderViewController()
    
    // MARK: Life Cycles
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //self.edgesForExtendedLayout = []
        self.updateTabBar()
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = "Home"
        self.updateContentContainerView()
    }
    
    override func viewDidLayoutSubviews() {
        self.navigationController!.navigationBar.isTranslucent = false
    }
    
    // MARK: Helpers
    func updateTabBar() {
        if self.activeTabBar == .machineData {
            self.machineDataButton.setTitleColor(UIColor.black, for: .normal)
            self.machineDataBottomView.isHidden = false
            self.codeReaderButton.setTitleColor(UIColor.lightGray, for: .normal)
            self.codeReaderBottomView.isHidden = true
        } else {
            self.machineDataButton.setTitleColor(UIColor.lightGray, for: .normal)
            self.machineDataBottomView.isHidden = true
            self.codeReaderButton.setTitleColor(UIColor.black, for: .normal)
            self.codeReaderBottomView.isHidden = false
        }
    }
    
    func add(asChildViewController viewController: UIViewController) {
        // Add Child View Controller
        addChild(viewController)
        
        // Add Child View as Subview
        self.contentContainerView.addSubview(viewController.view)
        
        // Configure Child View
        viewController.view.frame = self.contentContainerView.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        // Notify Child View Controller
        viewController.didMove(toParent: self)
    }
    
    func remove(asChildViewController viewController: UIViewController) {
        // Notify Child View Controller
        viewController.willMove(toParent: nil)
        
        // Remove Child View From Superview
        viewController.view.removeFromSuperview()
        
        // Notify Child View Controller
        viewController.removeFromParent()
    }
    
    func updateContentContainerView() {
        if self.activeTabBar == .machineData {
            remove(asChildViewController: self.codeReaderVC)
            add(asChildViewController: self.machineDataListVC)
        } else {
            remove(asChildViewController: self.machineDataListVC)
            add(asChildViewController: self.codeReaderVC)
        }
    }
    
    func updateView() {
        self.updateTabBar()
        self.updateContentContainerView()
    }
    
    // MARK: Actions
    @IBAction func machineDataButtonTapped(_ sender: UIButton) {
        self.activeTabBar = .machineData
        self.updateView()
    }
    
    @IBAction func codeReaderButtonTapped(_ sender: UIButton) {
        self.activeTabBar = .codeReader
        self.updateView()
    }
    
}
