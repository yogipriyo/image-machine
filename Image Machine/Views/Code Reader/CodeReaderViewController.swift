//
//  CodeReaderViewController.swift
//  Image Machine
//
//  Created by Yogi Priyo Prayogo on 11/12/18.
//  Copyright © 2018 Yogi Priyo Prayogo. All rights reserved.
//

import UIKit
import AVFoundation
import CoreData

class CodeReaderViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {

    // MARK: Properties
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    
    // MARK: Life Cycles
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.black
        captureSession = AVCaptureSession()
        
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }
        
        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }
        
        let metadataOutput = AVCaptureMetadataOutput()
        
        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr]
        } else {
            failed()
            return
        }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        view.layer.addSublayer(previewLayer)
        
        captureSession.startRunning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if (captureSession?.isRunning == false) {
            captureSession.startRunning()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
    }
    
    // MARK: Helpers
    func failed() {
        self.displayAlert(title: "Scanning not supported", content: "Your device does not support scanning a code from an item. Please use a device with a camera.")
        captureSession = nil
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSession.stopRunning()
        
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(code: stringValue)
        }
        
        //dismiss(animated: true)
    }
    
    func found(code: String) {
        self.searchMachineData(qrCodeNumber: code)
        print(code)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    func displayAlert(title: String, content: String) {
        let alert = UIAlertController(title: title, message: content, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        self.present(alert, animated: true)
    }

    func searchMachineData(qrCodeNumber: String){
        var machineData: MachineData?
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "MachineDatas")
        request.predicate = NSPredicate(format: "qrCodeNumber = %d", Int(qrCodeNumber)!)
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            if result.isEmpty == false {
                for data in result as! [NSManagedObject] {
                    //print(data.value(forKey: "name") as! String)
                    machineData = MachineData.init(
                        id: data.value(forKey: "id") as! Int,
                        name: data.value(forKey: "name") as! String,
                        type: data.value(forKey: "type") as! String,
                        qrCodeNumber: data.value(forKey: "qrCodeNumber") as! Int,
                        lastMaintenance: data.value(forKey: "lastMaintenance") as? Date
                    )
                }
                
                let machineDataDetailsVC = MachineDataDetailsViewController()
                machineDataDetailsVC.machineData = machineData
                self.navigationController?.pushViewController(machineDataDetailsVC, animated: true)
            } else {
                self.displayAlert(title: "Sorry", content: "Data Machine is not found!")
            }
            print(result)
        } catch {
            print("Failed")
        }
        //self.machineDataList = tempMachineDataList
    }
}
