//
//  ImageDetailsViewController.swift
//  Image Machine
//
//  Created by Yogi Priyo Prayogo on 16/12/18.
//  Copyright © 2018 Yogi Priyo Prayogo. All rights reserved.
//

import UIKit

class ImageDetailsViewController: UIViewController {
    
    // MARK: Properties
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var overlayView: UIView!
    
    var assetImage: UIImage?

    // MARK: Life Cycles
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.mainImage.image = self.assetImage
        
        // Add swipe down gesture to dismiss
        let swipeDownGesture = UISwipeGestureRecognizer(
            target: self,
            action: #selector(self.swipedDown(gesture:)))
        
        swipeDownGesture.direction = .down
        self.overlayView.addGestureRecognizer(swipeDownGesture)
    }
    
    // MARK: Actions
    @objc func swipedDown(gesture: UISwipeGestureRecognizer) {
        if gesture.direction == UISwipeGestureRecognizer.Direction.down {
            self.dismiss(animated: true)
        }
    }
    
}
