//
//  AssociatedImageTableViewCell.swift
//  Image Machine
//
//  Created by Yogi Priyo Prayogo on 13/12/18.
//  Copyright © 2018 Yogi Priyo Prayogo. All rights reserved.
//

import UIKit

class AssociatedImageTableViewCell: UITableViewCell {
    
    // MARK: properties
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var localIdentifier: UILabel!
    @IBOutlet weak var createdAt: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
