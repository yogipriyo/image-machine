//
//  EditMachineDataViewController.swift
//  Image Machine
//
//  Created by Yogi Priyo Prayogo on 17/12/18.
//  Copyright © 2018 Yogi Priyo Prayogo. All rights reserved.
//

import UIKit
import DatePickerDialog
import CoreData

class EditMachineDataViewController: UIViewController {
    
    // MARK: Properties
    @IBOutlet weak var machineIdLabel: UILabel!
    @IBOutlet weak var machineNameField: UITextField!
    @IBOutlet weak var machineTypeField: UITextField!
    @IBOutlet weak var lastMaintenanceField: UITextField!
    @IBOutlet weak var qrCodeNumberField: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var maintenanceDateOverlayButton: UIButton!
    
    var machineData: MachineData?
    var selectedDate: NSDate?
    
    // MARK: Life Cycles
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.saveButton.layer.borderWidth = 1
        self.saveButton.layer.borderColor = UIColor.black.cgColor
        self.saveButton.layer.cornerRadius = 5
        
        self.navigationItem.title = "Edit Machine Data Details"
        self.navigationController?.navigationBar.topItem?.title = ""
        
        self.populateContent()
    }

    // MARK: Helpers
    func populateContent() {
        self.machineIdLabel.text = "Machine ID : \(String(describing: self.machineData!.id))"
        self.machineNameField.text = machineData?.name
        self.machineTypeField.text = machineData?.type
        self.qrCodeNumberField.text = "\(String(describing: machineData!.qrCodeNumber))"
        if let lastMaintenanceDate = self.machineData?.lastMaintenance {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy"
            self.lastMaintenanceField.text = formatter.string(from: lastMaintenanceDate)
        }
    }
    
    func displayAlert(title: String, content: String) {
        let alert = UIAlertController(title: title, message: content, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            self.navigationController?.popToRootViewController(animated: true)
        }))
        
        self.present(alert, animated: true)
    }
    
    // MARK: Actions
    @IBAction func maintenanceDateOverlayButtonTapped(_ sender: UIButton) {
        DatePickerDialog().show("Date Picker", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .date) {
            (date) -> Void in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "dd/MM/yyyy"
                //print(dt)
                self.lastMaintenanceField.text = formatter.string(from: dt)
                self.selectedDate = dt as NSDate
            }
        }
    }
    
    @IBAction func saveButtonTapped(_ sender: UIButton) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "MachineDatas")
        fetchRequest.predicate = NSPredicate(format: "id = %d", self.machineData!.id)
        do {
            let test = try managedContext.fetch(fetchRequest)
            
            let objectUpdate = test[0] as! NSManagedObject
            objectUpdate.setValue(self.machineNameField.text!, forKey: "name")
            objectUpdate.setValue(self.machineTypeField.text!, forKey: "type")
            objectUpdate.setValue(Int(self.qrCodeNumberField.text!), forKey: "qrCodeNumber")
            //print(self.selectedDate)
            if let selectedDate = self.selectedDate {
                objectUpdate.setValue(selectedDate, forKey: "lastMaintenance")
            }
            do {
                try managedContext.save()
                self.displayAlert(title: "Success!", content: "Machine has been updated!")
            }
            catch
            {
                print(error)
            }
        }
        catch
        {
            print(error)
        }
    }
}
