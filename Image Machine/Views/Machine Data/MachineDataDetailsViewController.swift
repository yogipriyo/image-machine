//
//  MachineDataDetailsViewController.swift
//  Image Machine
//
//  Created by Yogi Priyo Prayogo on 13/12/18.
//  Copyright © 2018 Yogi Priyo Prayogo. All rights reserved.
//

import UIKit
import OpalImagePicker
import Photos
import Foundation
import CoreData
import Toast_Swift
import DZNEmptyDataSet

class MachineDataDetailsViewController: UIViewController, UITableViewDelegate,
UITableViewDataSource, OpalImagePickerControllerDelegate,
DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    // MARK: Properties
    @IBOutlet weak var machineDetailsContainerView: UIView!
    @IBOutlet weak var machineIdLabel: UILabel!
    @IBOutlet weak var machineNameLabel: UILabel!
    @IBOutlet weak var machineTypeLabel: UILabel!
    @IBOutlet weak var machineQRCodeNumberLabel: UILabel!
    @IBOutlet weak var machineMaintenanceLabel: UILabel!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var associatedImagesStatus: UILabel!
    @IBOutlet weak var associatedImagesTableView: UITableView!
    
    let tableCellIdentifier = "AssociatedImageCell"
    var machineData: MachineData?
    var associatedImageList: [UIImage] = []
    var associatedAssetId: [String] = []
    var associatedPhAsset: [PHAsset] = []
    var maxAllowedImages: Int = 10
    
    lazy var editDetailsButton: UIBarButtonItem = {
        var barButton = UIBarButtonItem.init(
            title: "Edit",
            style: .plain,
            target: self,
            action: #selector(editDetailsTapped))
        return barButton
    }()
    
    // MARK: Life Cyckes
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = "Machine Data Details"
        self.navigationController?.navigationBar.topItem?.title = ""
        navigationItem.rightBarButtonItems = [editDetailsButton]
        associatedImagesTableView.register(
            UINib(nibName: "AssociatedImageTableViewCell", bundle: nil),
            forCellReuseIdentifier: tableCellIdentifier)
        associatedImagesTableView.emptyDataSetSource = self
        associatedImagesTableView.emptyDataSetDelegate = self
        self.populateMachineDetails()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getAssociatedImages()
        self.associatedImagesStatus.text = "Associated Images (max : \(self.maxAllowedImages))"
    }

    // MARK: Helpers
    func populateMachineDetails() {
        if let machineData = self.machineData {
            self.machineIdLabel.text = "\(machineData.id)"
            self.machineNameLabel.text = machineData.name
            self.machineTypeLabel.text = machineData.type
            self.machineQRCodeNumberLabel.text = "\(machineData.qrCodeNumber)"
            if let lastMaintenanceDate = self.machineData?.lastMaintenance {
                let formatter = DateFormatter()
                formatter.dateFormat = "dd/MM/yyyy"
                self.machineMaintenanceLabel.text = formatter.string(from: lastMaintenanceDate)
            }
        }
    }
    
    func getUIImage(asset: PHAsset) -> UIImage? {
        var img: UIImage?
        let manager = PHImageManager.default()
        let options = PHImageRequestOptions()
        options.version = .original
        options.isSynchronous = true
        manager.requestImageData(for: asset, options: options) { data, _, _, _ in
            
            if let data = data {
                img = UIImage(data: data)
            }
        }
        return img
    }
    
    func displayAlert(title: String, content: String) {
        let alert = UIAlertController(title: title, message: content, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        self.present(alert, animated: true)
    }
    
    // MARK: Actions
    @IBAction func addImagesTapped(_ sender: UIButton) {
        guard UIImagePickerController.isSourceTypeAvailable(.photoLibrary) else {
            //Show error to user?
            return
        }
        
        //Example Instantiating OpalImagePickerController with Closures
        let imagePicker = OpalImagePickerController()
        imagePicker.maximumSelectionsAllowed = self.maxAllowedImages - self.associatedImageList.count
        imagePicker.imagePickerDelegate = self
        //Present Image Picker
        presentOpalImagePickerController(imagePicker, animated: true, select: { (assets) in
            //Save Images, update UI
            //Dismiss Controller
            if assets.isEmpty == false {
                for asset in assets {
                    let selectedImage = self.getUIImage(asset: asset)
                    self.associatedImageList.append(selectedImage!)
                    self.saveImage(selectedImageId: asset.localIdentifier)
                }
            }
            self.associatedImagesTableView.reloadData()
            imagePicker.dismiss(animated: true, completion: nil)
        }, cancel: {
            //Cancel action?
        })
    }
    
    func saveImage(selectedImageId: String) {
        // preparation
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "ImagesVault", in: context)
        let newImage = NSManagedObject(entity: entity!, insertInto: context)
        
        newImage.setValue("\(String(describing: self.machineData?.id))\(selectedImageId)", forKey: "id")
        newImage.setValue(self.machineData?.id, forKey: "machineId")
        newImage.setValue(selectedImageId, forKey: "assetId")
        
        do {
            try context.save()
            print("Image saving done!")
        } catch {
            print("Image saving fails!")
            //self.displayAlert(title: "Warning!", content: "Machine Data creation is failed!")
        }
    }
    
    func getAssociatedImages(){
        //print(self.machineData!.id)
        var tempAssociatedImages: [UIImage] = []
        var tempAssociatedAssetId: [String] = []
        var tempAssociatedPhAsset: [PHAsset] = []
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ImagesVault")
        request.predicate = NSPredicate(format: "machineId = %d", self.machineData!.id)
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                let requestOptions = PHImageRequestOptions()
                requestOptions.isSynchronous = false
                let asset = PHAsset.fetchAssets(withLocalIdentifiers: [data.value(forKey: "assetId") as! String], options: .none).firstObject
                //print(asset)
                let singleImage = self.getUIImage(asset: asset!)
                tempAssociatedImages.append(singleImage!)
                tempAssociatedAssetId.append(data.value(forKey: "id") as! String)
                tempAssociatedPhAsset.append(asset!)
            }
            //print(result)
        } catch {
            print("Failed")
        }
        self.associatedPhAsset = tempAssociatedPhAsset
        self.associatedAssetId = tempAssociatedAssetId
        self.associatedImageList = tempAssociatedImages
        self.associatedImagesTableView.reloadData()
    }

    func deleteImage(selectedIndex: Int) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let deleteRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "ImagesVault")
        deleteRequest.predicate = NSPredicate(format: "id = %@", "\(self.associatedAssetId[selectedIndex])")
        deleteRequest.returnsObjectsAsFaults = false
        
        do {
            let arrUsrObj = try context.fetch(deleteRequest)
            //print(arrUsrObj)
            for usrObj in arrUsrObj as! [NSManagedObject] { // Fetching Object
                context.delete(usrObj) // Deleting Object
            }
        } catch {
            print("Failed")
        }
        
        // Saving the Delete operation
        do {
            try context.save()
            self.displayAlert(title: "Attention!", content: "Image has been deleted!")
        } catch {
            print("Failed saving")
        }
    }
    
    @objc func editDetailsTapped() {
        let editDetailsVC = EditMachineDataViewController()
        editDetailsVC.machineData = self.machineData
        self.navigationController?.pushViewController(editDetailsVC, animated: true)
    }
    
    // MARK: Table View Data Source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.associatedImageList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: tableCellIdentifier)
            as! AssociatedImageTableViewCell
        cell.mainImage.image = self.associatedImageList[indexPath.row]
        cell.localIdentifier.text = self.associatedPhAsset[indexPath.row].localIdentifier
        cell.createdAt.text = "\(String(describing: self.associatedPhAsset[indexPath.row].creationDate!))"
        cell.selectionStyle = .none
        return cell
    }
    
    // MARK: Table View Delegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.deleteImage(selectedIndex: indexPath.row)
            self.associatedImageList.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let imageDetailsVC = ImageDetailsViewController()
        imageDetailsVC.modalPresentationStyle = .overCurrentContext
        imageDetailsVC.assetImage = self.associatedImageList[indexPath.row]
        self.navigationController?.present(imageDetailsVC, animated: true, completion: nil)
    }
    
    // MARK: DZN Empty Data Set Source
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let text = "This machine doesn't have any images yet!"
        let attributedString = NSMutableAttributedString(string: text)
        
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor,
                                      value: UIColor.black,
                                      range: NSRange.init(location: 0, length: text.count))
        
        return attributedString
    }

    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage]) {
        //Save Images, update UI
        //Dismiss Controller
        presentedViewController?.dismiss(animated: true, completion: nil)
    }
}
