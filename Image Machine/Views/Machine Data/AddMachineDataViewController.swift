//
//  AddMachineDataViewController.swift
//  Image Machine
//
//  Created by Yogi Priyo Prayogo on 12/12/18.
//  Copyright © 2018 Yogi Priyo Prayogo. All rights reserved.
//

import UIKit
import CoreData

class AddMachineDataViewController: UIViewController {

    // MARK: Properties
    @IBOutlet weak var machineIdLabel: UILabel!
    @IBOutlet weak var machineNameTextField: UITextField!
    @IBOutlet weak var machineTypeTextField: UITextField!
    @IBOutlet weak var QRCodeNumberTextField: UITextField!
    @IBOutlet weak var addMachineButton: UIButton!
    var machineId: Int = 0
    var takenMachineId: [Int] = []
    
    // MARK: Life Cycles
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = "Add Machine Data"
        self.navigationController?.navigationBar.topItem?.title = ""
        self.setupButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.generateRandomNumber()
    }
    
    // MARK: Helpers
    func generateRandomNumber() {
        let number = Int.random(in: 0 ... 10000000)
        if self.takenMachineId.index(of: number) != nil {
            self.generateRandomNumber()
        } else {
            self.machineId = number
            self.machineIdLabel.text = "Machine ID : \(number)"
        }
    }
    
    func setupButton() {
        self.addMachineButton.layer.borderWidth = 1
        self.addMachineButton.layer.borderColor = UIColor.black.cgColor
        self.addMachineButton.layer.cornerRadius = 5.0
    }
    
    func validateForm() -> Bool {
        var validatePassed: Bool = false
        
        let textFieldArray: [UITextField] = [self.machineNameTextField, self.machineTypeTextField, self.QRCodeNumberTextField]
        for textField in textFieldArray {
            if textField.text != "" {
                validatePassed = true
            } else {
                validatePassed = false
                break
            }
        }
        //print(validatePassed)
        return validatePassed
    }
    
    func displayAlert(title: String, content: String) {
        let alert = UIAlertController(title: title, message: content, preferredStyle: .alert)
        
        if title == "Sorry!" {
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        } else {
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                self.navigationController?.popToRootViewController(animated: true)
            }))
        }
        
        self.present(alert, animated: true)
    }
    
    // MARK: Actions
    @IBAction func addMachineButtonTapped(_ sender: UIButton) {
        let formValidated = self.validateForm()
        if formValidated == true {
            self.addMachineData()
        } else {
            self.displayAlert(title: "Sorry!", content: "Please fill all the fields.")
        }
    }
    
    func addMachineData() {
        // preparation
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "MachineDatas", in: context)
        let newMachineData = NSManagedObject(entity: entity!, insertInto: context)
        
        newMachineData.setValue(self.machineId, forKey: "id")
        newMachineData.setValue(self.machineNameTextField.text, forKey: "name")
        newMachineData.setValue(self.machineTypeTextField.text, forKey: "type")
        newMachineData.setValue(Int(self.QRCodeNumberTextField.text ?? "0"), forKey: "qrCodeNumber")
        newMachineData.setValue(nil, forKey: "lastMaintenance")
        
        do {
            try context.save()
            self.displayAlert(title: "Success!", content: "Machine Data is created!")
        } catch {
            self.displayAlert(title: "Sorry!", content: "Machine Data creation is failed!")
        }
    }
}
