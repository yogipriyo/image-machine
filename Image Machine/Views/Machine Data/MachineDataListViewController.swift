//
//  MachineDataListViewController.swift
//  Image Machine
//
//  Created by Yogi Priyo Prayogo on 11/12/18.
//  Copyright © 2018 Yogi Priyo Prayogo. All rights reserved.
//

import UIKit
import CoreData
import DZNEmptyDataSet

struct MachineData {
    var id: Int = 0
    var name: String = ""
    var type: String = ""
    var qrCodeNumber: Int = 0
    var lastMaintenance: Date?
}

class MachineDataListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,
DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    // MARK: Properties
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var machineDataTableView: UITableView!
    @IBOutlet weak var addMachineDataButton: UIButton!
    @IBOutlet weak var sortByButton: UIButton!
    
    let tableCellIdentifier = "MachineDataCell"
    var machineDataList: [MachineData] = []
    var takenMachineId: [Int] = []

    // MARK: Life Cycles
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        machineDataTableView.register(
            UINib(nibName: "MachineDataTableViewCell", bundle: nil),
            forCellReuseIdentifier: tableCellIdentifier)
        machineDataTableView.emptyDataSetSource = self
        machineDataTableView.emptyDataSetDelegate = self

        self.setupButtons()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getMachineDatas()
        
    }
    
    // MARK: Helpers
    func setupButtons() {
        let buttonArray: [UIButton] = [self.addMachineDataButton, self.sortByButton]
        for button in buttonArray {
            button.layer.borderWidth = 1
            button.layer.borderColor = UIColor.black.cgColor
            button.layer.cornerRadius = 5.0
        }
    }
    
    func displayAlert(title: String, content: String) {
        let alert = UIAlertController(title: title, message: content, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        self.present(alert, animated: true)
    }
    
    func getMachineDatas() {
        var tempMachineDataList: [MachineData] = []
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "MachineDatas")
        //request.predicate = NSPredicate(format: "age = %@", "12")
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                //print(data.value(forKey: "name") as! String)
                let machineData = MachineData.init(
                    id: data.value(forKey: "id") as! Int,
                    name: data.value(forKey: "name") as! String,
                    type: data.value(forKey: "type") as! String,
                    qrCodeNumber: data.value(forKey: "qrCodeNumber") as! Int,
                    lastMaintenance: data.value(forKey: "lastMaintenance") as? Date
                )
                tempMachineDataList.append(machineData)
                self.takenMachineId.append(machineData.id)
            }
            //print(result)
        } catch {
            print("Failed")
        }
        self.machineDataList = tempMachineDataList
        self.machineDataTableView.reloadData()
    }
    
    // MARK: Actions
    @IBAction func addMachineDataTapped(_ sender: UIButton) {
        let addMachineDataVC = AddMachineDataViewController()
        addMachineDataVC.takenMachineId = self.takenMachineId
        self.navigationController?.pushViewController(addMachineDataVC, animated: true)
    }
    
    @IBAction func sortByTapped(_ sender: UIButton) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let sortNameAsc = UIAlertAction(title: "Machine Name (ASC)", style: .default, handler: { (alert: UIAlertAction!) -> Void in
            self.machineDataList = self.machineDataList.sorted(by: { $0.name < $1.name })
            self.machineDataTableView.reloadData()
        })
        
        let sortNameDesc = UIAlertAction(title: "Machine Name (DESC)", style: .default, handler: { (alert: UIAlertAction!) -> Void in
            self.machineDataList = self.machineDataList.sorted(by: { $0.name > $1.name })
            self.machineDataTableView.reloadData()
        })
        
        let sortTypeAsc = UIAlertAction(title: "Machine Type (ASC)", style: .default, handler: { (alert: UIAlertAction!) -> Void in
            self.machineDataList = self.machineDataList.sorted(by: { $0.type < $1.type })
            self.machineDataTableView.reloadData()
        })
        
        let sortTypeDesc = UIAlertAction(title: "Machine Type (DESC)", style: .default, handler: { (alert: UIAlertAction!) -> Void in
            self.machineDataList = self.machineDataList.sorted(by: { $0.type > $1.type })
            self.machineDataTableView.reloadData()
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (alert: UIAlertAction!) -> Void in
            //  Do something here upon cancellation.
        })
        
        alertController.addAction(sortNameAsc)
        alertController.addAction(sortNameDesc)
        alertController.addAction(sortTypeAsc)
        alertController.addAction(sortTypeDesc)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func deleteMachine(selectedIndex: Int) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let deleteRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "MachineDatas")
        deleteRequest.predicate = NSPredicate(format: "id = %d", self.machineDataList[selectedIndex].id)
        deleteRequest.returnsObjectsAsFaults = false
        
        do {
            let arrUsrObj = try context.fetch(deleteRequest)
            for usrObj in arrUsrObj as! [NSManagedObject] { // Fetching Object
                context.delete(usrObj) // Deleting Object
            }
        } catch {
            print("Failed")
        }
        
        // Saving the Delete operation
        do {
            try context.save()
            self.displayAlert(title: "Attention!", content: "Machine data has been deleted!")
        } catch {
            print("Failed saving")
        }
    }
    
    // MARK: Table View Data Source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.machineDataList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: tableCellIdentifier)
            as! MachineDataTableViewCell
        cell.machineDataName.text = self.machineDataList[indexPath.row].name
        cell.machineDataId.text = "ID: \(self.machineDataList[indexPath.row].id)"
        cell.machineDataType.text = "Type: \(self.machineDataList[indexPath.row].type)"
        cell.selectionStyle = .none
        
        return cell
    }
    
    // MARK: Table View Delegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }
    
    func tableView(_ tableView: UITableView,
                   commit editingStyle: UITableViewCell.EditingStyle,
                   forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.deleteMachine(selectedIndex: indexPath.row)
            self.machineDataList.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let machineDataDetailsVC = MachineDataDetailsViewController()
        machineDataDetailsVC.machineData = self.machineDataList[indexPath.row]
        self.navigationController?.pushViewController(machineDataDetailsVC, animated: true)
    }
    
    // MARK: DZN Empty Data Set Source
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let text = "You haven't create any machine data yet!"
        let attributedString = NSMutableAttributedString(string: text)

        attributedString.addAttribute(NSAttributedString.Key.foregroundColor,
                                      value: UIColor.black,
                                      range: NSRange.init(location: 0, length: text.count))

        return attributedString
    }
}
